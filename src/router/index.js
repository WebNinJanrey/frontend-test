import Vue from 'vue'
import Router from 'vue-router'
import DefaultContainer from "../components/DefaultContainer.vue"
import Category from "../components/Category/index.vue"
import Articles from "../components/Articles/index.vue"

Vue.use(Router)

function configRoutes() {
    return [
        {
            path: '/',
            redirect: '/categories',
            name: 'Categories',
            component: DefaultContainer,
            children: [
                {
                    path: 'categories',
                    name: 'Categories',
                    component: Category
                },
                {
                    path: '/category/:id',
                    name: 'View Articles',
                    component: Articles,
                },
            ]
        }
    ]
}

export default new Router({
    mode: 'hash', // https://router.vuejs.org/api/#mode
    linkActiveClass: 'open active',
    routes: configRoutes()
})